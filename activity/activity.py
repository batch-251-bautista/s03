# 1. Create a Class called Camper and give it the attributes name, batch, course_type
# 2. Create a method called career_track which will print out the string 'Currently enrolled in the <value of course_type> program'
# 3. Create a method called info which will print out the string 'My name is <value of name> of batch <value of batch>'
# 4. Create an object from class Camper called zuitt_camper and pass in arguments for name, batch, and course_type

class Camper():
	def __init__(self, name, batch, course_type):
		self.name = name
		self.batch = batch
		self.course_type = course_type

	def career_track(self):
		print(f"Currently enrolled in the {self.course_type} program")

	def info(self):
		print(f"My name is {self.name} of batch {self.batch}")

newCamper = Camper("Edmar", "251", "Python short course")

# Print the value of the objects name, batch, course type.
def printDetails(person):
	print(f"Camper Name: {person.name}")
	print(f"Camper Batch: {person.batch}")
	print(f"Camper Course: {person.course_type}")

printDetails(newCamper)

# Execute the info method and career_track of the object
newCamper.info()
newCamper.career_track()
